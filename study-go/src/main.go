package main

import (
	"datastruct"
	"fmt"
)

func main() {
	//var stack []int
	//
	//for i := 1; i <= 5; i++ {
	//	stack = append(stack, i)
	//}
	//fmt.Println(stack)
	//
	//for len(stack) > 0 {
	//	var last int
	//	last, stack = stack[len(stack)-1], stack[:len(stack)-1]
	//	fmt.Println(last)
	//}
	//
	//var queue []int
	//for i := 1; i <= 5; i++ {
	//	queue = append(queue, i)
	//}
	//fmt.Println(queue)
	//
	//for len(queue) > 0 {
	//	var first int
	//	first, queue = queue[0], queue[1:]
	//	fmt.Println(first)
	//}

	stack := datastruct.NewStack()
	for i := 1; i <= 5; i++ {
		stack.Push(i)
	}
	for !stack.IsEmpty() {
		data := stack.Pop()
		fmt.Printf("%d =>", data)
	}

	fmt.Println()

	queue := datastruct.NewQueue()
	for i := 1; i <= 5; i++ {
		queue.Push(i)
	}
	for !queue.IsEmpty() {
		data := queue.Pop()
		fmt.Printf("%d =>", data)
	}
}