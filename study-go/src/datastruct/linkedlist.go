package datastruct

import "fmt"

type LinkedList struct {
	Root *Node
	Tail *Node
}

func (l *LinkedList) AddNode(Data int) {
	if l.Root == nil {
		l.Root = &Node{Data: Data}
		l.Tail = l.Root
		return
	}
	l.Tail.Next = &Node{Data: Data}
	l.Tail = l.Tail.Next
}

func (l *LinkedList) PopBack() {
	if l.Tail == nil {
		return
	}
	l.RemoveNode(l.Tail)
}

func (l *LinkedList) Back() int {
	if l.Tail != nil {
		return l.Tail.Data
	}
	return 0
}

func (l *LinkedList) PopFront() {
	if l.Root == nil {
		return
	}
	l.RemoveNode(l.Root)
}

func (l *LinkedList) Front() int {
	if l.Root != nil {
		return l.Root.Data
	}
	return 0
}

func (l *LinkedList) IsEmpty() bool {
	return l.Root == nil
}

func (l *LinkedList) RemoveNode(node *Node) {
	if node == l.Root {
		l.Root = l.Root.Next
		node.Next = nil
		return
	}

	// 삭제하려는 node 가 처음이 아닌 경우는
	// 삭제하려는 node 의 '이전 node' 위치를 찾아야 한다
	prev := l.Root
	for prev.Next != node {
		prev = prev.Next
	}

	// 삭제하려는 node 가 '맨 마지막 node' 인 경우와
	// 삭제하려는 node 가 '중간에 위치한 node' 인 경우로 나눌 수 있다
	if node == l.Tail {
		prev.Next = nil
		l.Tail = prev
	} else {
		prev.Next = prev.Next.Next
	}

	node.Next = nil
}

func (l *LinkedList) PrintNode()  {
	node := l.Root
	for node.Next != nil {
		fmt.Printf("%d ->", node.Data)
		node = node.Next
	}
	fmt.Printf("%d\n", node.Data)
}

