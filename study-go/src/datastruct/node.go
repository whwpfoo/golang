package datastruct

type Node struct {
	Next *Node
	Prev *Node
	Data int
}
