package datastruct

type Stack struct {
	ll *LinkedList
}

func NewStack() *Stack {
	return &Stack{ll:&LinkedList{}}
}

func (s *Stack) Push(data int) {
	s.ll.AddNode(data)
}

func (s *Stack) Pop() int {
	back := s.ll.Back()
	s.ll.PopBack()
	return back
}

func (s *Stack) IsEmpty() bool {
	return s.ll.IsEmpty()
}