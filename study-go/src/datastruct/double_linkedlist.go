package datastruct

import "fmt"

type DoubleLinkedList struct {
	Root *Node
	Tail *Node
}

func (l *DoubleLinkedList) AddNode(Data int) {
	if l.Root == nil {
		l.Root = &Node{Data: Data}
		l.Tail = l.Root
		return
	}
	l.Tail.Next = &Node{Data: Data}
	Prev := l.Tail
	l.Tail = l.Tail.Next
	l.Tail.Prev = Prev
}

func (l *DoubleLinkedList) RemoveNode(node *Node) {
	if node == l.Root {
		l.Root = l.Root.Next
		l.Root.Prev = nil
		node.Next = nil
		return
	}

	// 삭제하려는 node 가 처음이 아닌 경우는
	// 삭제하려는 node 의 '이전 node' 위치를 찾아야 한다
	Prev := node.Prev

	// 삭제하려는 node 가 '맨 마지막 node' 인 경우와
	// 삭제하려는 node 가 '중간에 위치한 node' 인 경우로 나눌 수 있다
	if node == l.Tail {
		Prev.Next = nil
		l.Tail.Prev = nil
		l.Tail = Prev
	} else {
		node.Prev = nil
		Prev.Next = Prev.Next.Next
		Prev.Next.Prev = Prev
	}

	node.Next = nil
}

func (l *DoubleLinkedList) PrintNode()  {
	node := l.Root
	for node.Next != nil {
		fmt.Printf("%d ->", node.Data)
		node = node.Next
	}
	fmt.Printf("%d\n", node.Data)
}
