package main

import "fmt"

/*
	"slice" 란 동적배열을 뜻한다
	동적배열은 배열의 길이가 동적으로 변할 수 있는 배열을 말한다
	길이는 기존의 2배씩 공간을 추가하게 된다
 */
func main() {
	var a []int
	fmt.Printf("len(a) = %d\n", len(a))
	fmt.Printf("cap(a) = %d\n", cap(a))

	b := []int{1, 2, 3, 4}
	fmt.Printf("len(a) = %d\n", len(b))
	fmt.Printf("cap(a) = %d\n", cap(b))

	c := make([]int, 0, 8)
	fmt.Printf("len(a) = %d\n", len(c))
	fmt.Printf("cap(a) = %d\n", cap(c))

	/*
		주의점으로, slice 의 capacity 여유가 있는 상태에서 append() 한 경우는 리턴되는
		slice 객체는 인자로 넘긴 slice 객체가 리턴되고
		slice 의 capacity 여유가 없는 상태에서 append() 한 경우
		리턴되는 slice 객체 새로운 객체가 생성되어 리턴된다
	*/
	c = append(c, 5)
	fmt.Println(c)

	/*
		slice 일부분을 추출하여 새로운 변수에 값을 저장한다
		저장한 변수를 이용해 slice 값을 변경하면 원본 slice 데이터에도 적용이 된다
	*/
	d := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	f := d[5:9]
	f[0] = 0 
	f[1] = 0 
	f[2] = 0
	fmt.Println(d)
	fmt.Println(f)
}

/*
	Slice 도 Struct 이다
	type Slice stuct {
		pointer 	*[]T	// 시작주소
		len			int		// 데이터의 갯수
		capacity	int		// 데이터의 최대 갯수
	}
 */