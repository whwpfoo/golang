/*
	'main' 이라는 패키지 선언
	'main' 은 모든 파일의 시작점이라는 뜻 (필수적임)
 */
package main

import (
	"fmt"
)

/*
	func 정의(입력) 출력 {
		함수 시작
	}
*/
func main() {
	fmt.Println("Hello World Go Language")
}