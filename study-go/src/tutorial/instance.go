package main

import "fmt"

type Student struct {
	name	string
	age		int
	grade	int
}

func (t *Student) SetName(newName string) {
	t.name = newName
}

func main() {
	a := Student{"aaa", 20, 10}
	//b := a
	//b := &a
	//b.name = "bbb"
	//b.age = 30
	//
	//fmt.Println(a)
	//fmt.Println(*b)
	fmt.Println(a)
	a.SetName("bbb")
	fmt.Println(a)
}

