package main

import (
	"fmt"
)

type Person struct {
	name	string
	age		int
}

func (p Person) PrintName() {
	fmt.Println(p.name)
}

type Student struct {
	name	string
	class	int
	score	Score
}

type Score struct {
	score	int
}

func (t *Student) GetStudentInfo() {
	fmt.Println(*t)
}

func (t *Student) ChangeScore(score int) {
	t.score.score = 100
}

func main() {
	//var p Person
	//p1 := Person{"개똥이", 15}
	//p2 := Person{name: "개똥이"}
	//p.name = "안녕하세요 ㅋ"
	//p.age = 255
	//fmt.Println(p, p1, p2)
	//p.PrintName()

	st := Student{"whwpfoo", 1, Score{90}}
	st.GetStudentInfo()
	st.ChangeScore(100)
	st.GetStudentInfo()

}