package main

import "fmt"

func sumToNum(a int16, b int16) int16 {
	return a + b
}

func main() {
	var a int16
	var b int16
	a = 50
	b = 50
	fmt.Println(sumToNum(a, b))
}

/*
	변수 => variable '변하는 숫자'
			변수는 '메모리 번지' 를 가지고 있다
	형식 =>
			1) '이름' '값' '타입'
				var		a	int
	타입 =>
			int)8bit	1byte
				int 			(컴퓨터 bit 수에 따라 달라짐)
				int32	4byte	(-21억	~	21억)
				uint32			(-42억	~	42억)
				int64	8byte
				uint64
				int8	1byte	(-128	~	127)
				uint8			(0		~	255)
				int16	2byte	(-32768	~	32767)
				uint16			(0		~ 	65535)
			float)
				'정수부', '실수부' 로 표현되는 숫자
				float32	4byte	'숫자' 부분을 7 자리까지 표현
				float64	8byte	'숫자' 부분을 15 자리까지 표현
			string)
				rune 들의 모임이다 "letters"
				'l' 'e' 't' 't' ... 각 각을 'rune' 이라고 한다
			boolean)
*/