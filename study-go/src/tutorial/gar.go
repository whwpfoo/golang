package main

/*
	"변수" 는 메모리다
	"변수" 는 값을 담는 그릇이다
	쓰이지 않는 "변수" 는 Memory Garbage 이다
	"Memory leak" 은 일종의 버그
	golang 에서 프로그래머는 "스택 메모리", "힙 메모리" 신경을 쓰지 않아도 됨

	"GC" 는 Reference Count == 0 인 애들을 찾아서 메모리 제거한다
	뿐만 아니라 "외딴섬" 구조의 메모리를 찾아서 메모리를 제거한다

	"GC" 는 수행작업이 많으므로 속도가 느리다
 */

func add() *int {
	var a int; a = 3
	var p *int; p = &a
	return p
}