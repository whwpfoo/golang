package main

import (
	"fmt"
	"strings"
)

func main() {
	/*for i := 1; i < 10; i++ {
		for j := 1; j < 10; j++ {
			if j == 3 && i == 2 {
				fmt.Printf("%d x %d = %3v   ", j, i, "p")
				continue
			}
			fmt.Printf("%d x %d = %3d   ", j, i, j * i)
		}
		fmt.Println()
	}*/

	/*for i := 1; i <= 5; i++ {
		fmt.Println(strings.Repeat("*", i))
	}*/
	whiteSpace := 3; star := 1; idx := 0
	for idx < 7 {
		printLetterForRepeat(" ", whiteSpace)
		printLetterForRepeat("*", star)
		fmt.Println()
		if idx < 3 {
			whiteSpace -= 1
			star += 2
		} else {
			whiteSpace += 1
			star -= 2
		}
		idx++
	}
}

func printLetterForRepeat(letter string, count int) {
	fmt.Print(strings.Repeat(letter, count))
}
