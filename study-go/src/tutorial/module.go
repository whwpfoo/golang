package main

import "fmt"

func main() {
	var num int; num = 1; p := &num
	*p = 3
	fmt.Println("num value => ", num, "num value changed...")
	num = 5
	fmt.Println("num value =>", num, "p value is => ", *p)

	//number := 1
	//Increase(&number)
	//fmt.Println(number)

	s := Student{name: "whwp", age: 10, class: "Math", grade: "A"}
	fmt.Println(s)

	//s.ChangeClassAndGrade("Eng", "D")
	s.ChangeClassAndGrade("Eng", "D")
	fmt.Println(s)

}

func Increase(num *int) {
	*num++
}

type Student struct {
	name	string
	age		int
	class	string
	grade 	string
}

func (s *Student) ChangeClassAndGrade(class string, grade string)  {
	fmt.Println("Student Pointer is ", s)
	s.class = class
	s.grade = grade
}