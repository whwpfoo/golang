package main

import (
	"fmt"
)

func main() {
	var A [10]int
	//A := [10]int {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	for i := 0; i < 10; i++ {
		A[i] = i * i
	}
	fmt.Println(A)

	s := "hello world"
	for i := 0; i < len(s); i++ {
		fmt.Print(s[i] - 'a', ", ")
	}

	//f := "헬로우 월드"
	//fa := []rune(f)
	//fmt.Println(len(fa))
	//for i := 0; i < len(fa); i++ {
	//	fmt.Print(string(fa[i]), ", ")
	//}

	fmt.Println()
	arr := [5]int{1, 2, 3, 4, 5}
	clone := [5]int{}

	for i := 0; i < 5; i++ {
		clone[i] = arr[i]
	}

	//fmt.Println(clone)
	for i := 0; i < len(arr)/2; i++ {
		arr[i], arr[len(arr) - 1 - i] = arr[len(arr) - 1 - i], arr[i]
	}
	fmt.Println(arr)

	// radis sort
	numbers := [10]int{5, 2, 3, 2, 1, 4, 5, 9, 8, 2}
	temp := [10]int{}

	for i := 0; i< len(numbers); i++ {
		idx := numbers[i]
		temp[idx]++
	}

	index := 0
	for i := 0; i < len(temp); i++ {
		for j := 0; j < temp[i]; j++ {
			numbers[index] = i
			index++
		}
	}

	fmt.Println(numbers)

}
