package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Result struct {
	strikes	int
	balls	int
}

func main() {
	rand.Seed(time.Now().UnixNano())
	numbers := MakeNumber()
	fmt.Println("computers number", numbers)

	for {
		userNumbers := InputNumber()
		fmt.Println("user input number", userNumbers)

		result := CompareNumber(numbers, userNumbers)
		PrintResult(result)
		if IsGameEnd(result) {
			break
		}
	}
}

// 무작위 랜덤한 숫자 3개를 생성하여 배열로 리턴
func MakeNumber() [3]int {
	var result [3]int
	for i := 0; i < 3; i++ {
		for {
			ranNum := rand.Intn(10)
			duplicated := false
			for j := 0; j < i; j++ {
				if result[j] == ranNum {
					duplicated = true
					break
				}
			}
			if !duplicated {
				result[i] = ranNum
				break
			}
		}
	}
	return result
}

func InputNumber() [3]int {
	var result [3]int
	var no int

	for {
		_, err := fmt.Scanf("%d\n", &no)
		if err != nil {
			fmt.Println("입력이 올바르지 않습니다")
			continue
		}

		success := true
		idx := 0

		for no > 0 {
			n := no % 10
			no = no / 10
			duplicated := false

			for j := 0; j < idx; j++ {
				if result[j] == n {
					duplicated = true
					break
				}
			}

			if duplicated {
				success = false
				fmt.Println("겹치지 않는 숫자를 입력해주세요")
				break
			}

			result[idx] = n;
			idx++
		}

		if !success {
			continue
		}

		break
	}

	result[0], result[2] = result[2], result[0]
	return result
}

func CompareNumber(numbers, userNumbers [3]int) Result {
	strikes := 0
	balls := 0

	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if numbers[i] == userNumbers[i] {
				if i == j {
					strikes++
				} else {
					balls++
				}
				break
			}
		}
	}

	return Result{strikes, balls}
}

func PrintResult(result Result) {
	fmt.Printf("%dB %dS\n", result.balls, result.strikes)
}

func IsGameEnd(result Result) bool {
	if result.strikes == 3 {
		return true
	}
	return false
}