package main

import (
	"fmt"
)

func main()  {
	a := 4
	b := 2
	fmt.Printf("a&b => %v\n", a&b) // %v 타입에 맞춰서 자동으로 출력
	fmt.Printf("a|b => %v\n", a|b)
	fmt.Printf("a^b => %v\n", a^b)

	c := 4
	fmt.Println(c << 1)
	fmt.Println(c >> 1)

	d := 5
	fmt.Println(d << 4)

	f := 3
	if f == 3 {
		fmt.Println("f의 값은 3")
	}
	fmt.Println("f 는", f)

	if f == 5 || f == 4 {
		fmt.Println("")
	} else {
		fmt.Println("f 는 4 도 아니고 5도 아니다")
	}
}

/*
	1) 	산술 연산자
	2)	비트 연산자
			'&'	and
			'|'	or
			'^' xor
	3)	논리 연산자
			연산의 결과가 'true' or 'false' 반환
	4)	그 외
	5) 조건문
			if '조건문' {

			}
*/