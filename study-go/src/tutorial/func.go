package main

import (
	"fmt"
)


func main() {
	for i := 0; i < 10; i++ {
		fmt.Printf("%d + %d = %d\n", i, i + 1, add(i, i + 1))
	}

	a, b := swap(1, 2)
	fmt.Printf("a 는 %d 이고\n b는 %d 입니다", a, b)

	c := 10; d := 10
	fmt.Println(c, d)
}

func add(x int, y int) int { // 파라미터는 무조건 '값 복사' 이다
	return x + y
}

func swap(x, y int) (int, int) {
	return y, x
}

func recur(x int) int {
	if x == 1 {
		return 1
	}
	return x * recur(x - 1)
}

func fibo(x int) int {
	if x == 0 || x == 1 {
		return 1
	}
	return fibo(x - 1) + fibo(x - 2)
}

/*
	함수
 */